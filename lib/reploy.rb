require 'fileutils'
require 'net/ssh'
require 'popen4'

=begin
	Reploy is simple deploying tool, similar to Vlad or Capistrano, but even simplier and faster.

	How to use:

	* Install a gem
	* Write a config
	* rake something
=end

class Reploy
	attr_accessor :current

	def config
		@@config
	end

	def config=(v)
		@@config=(v)
	end

	def self.[](k)
		@@config[k]
	end

	def self.[]=(k,v)
		@@config[k] = v
	end

	# load Reploy with given config
	def Reploy.load(opts)
		@@config = opts
		@@config.default = {}
		if opts[:recipes].is_a? Array
			opts[:recipes].each do |r|
				if r =~ /\.rb$/ # It's local
					require r
				else
					require "reploy/recipes/#{r}"
				end
			end
		end
		@@singleton = Reploy.new
		@@singleton.current = "#{@@config[:path]}/current"
		self.add_tasks
	end

	def self.add_tasks
		namespace :reploy do
			# tasks for calling reploy from rake
			# we can't (properly) override rake tasks, so it's methods. :(
			%w|setup transfer deploy start stop restart nuke|.each do |t|
				task t.to_sym do
					self.send(t.to_sym)
				end
			end
		end
	end

	def self.method_missing(s, *args, &block)
		if args.empty?
			@@singleton.send(s)
		else
			@@singleton.send(s, args)
		end
	end

	def ssh
		@ssh ||= spawn_ssh
	end

	def spawn_ssh
		begin
			Net::SSH.start(config[:host],config[:user])
		rescue Exception => e
			raise "Connecting via SSH failed: #{e.class} - #{e}! Check your settings."
		end
	end

	# Run the command on remote host
	def run(command)
		puts "#{config[:user]}@#{config[:host]}# #{command}"
		ssh.open_channel do |chan|
			chan.on_request("exit-status") do |ch,data|
				r = data.read_long
				if r != 0
					raise "Process failed with exit status #{r}"
				end
			end
			chan.on_data do |ch, data|
				data.each_line do |l|
					puts "[r] #{l}"
				end
			end
			chan.on_extended_data do |ch, type, data|
				if type == 1
					data.each_line do |l|
						puts "[r] [e] #{l}"
					end
				end
			end
			chan.exec command do |ch, success|
				raise "Running command failed." unless success
			end
			chan.wait
		end
		@ssh.loop
	end

	# Run the command on remote host, in "current" directory
	def run_in_current(command)
		run("cd #{@current} && #{command}")
	end

	# Create essential dir structure on remote host
	def setup
		run("mkdir -p "+(%w|current shared/log shared/tmp backups storage|.map {|v| "#{config[:path]}/#{v}"}.join(" ")))
	end

	# Check out all the files to be bulk-copied in directory, return its path
	# This is naive implementation, this method is overridden in recipes like "git", which use version control to provide filelist
	def checkout
		yield '.' # dumb
	end

	# Run the command on local host
	def run_local(command)
		puts "localhost# #{command}"
		status = POpen4::popen4 command do |stdout,stderr|
			stdout.readlines.each do |l|
				puts "[l] #{l}"
			end
			stderr.readlines.each do |l|
				puts "[l] [e] #{l}"
			end
		end
		raise "Process aborted in totally wrong way" if status.exitstatus != 0
	end

	# Prepare for transfer: create random named directory wih contents of current
	def prepare
		@current = "#{config[:path]}/tmp#{rand(66666)}"
		run "cp -r #{config[:path]}/current #{@current}"
	end

	# Perform the file transfer: prepare, then sync, then copy files, then rename directory to current
	# In case anything goes wrong, rollback and remove traces
	def transfer
		begin
			prepare
			checkout do |dir|
				sync dir
			end
			copy
			finalize
		rescue Exception => e
			puts "Exception: #{e}. Cleaning up..."
			cleanup
		end
	end

	# Finalize file transfer: rename temporary directory to current, remove or backup original "current"
	def finalize
		if config[:leave_backups] == true
			run "mv #{config[:path]}/current #{config[:path]/backups}/#{Time.now.strftime("%d_%m_%Y_%H_%M")} && mv #{@current} #{config[:path]}/current"
		else
			run "rm -r #{config[:path]}/current && mv #{@current} #{config[:path]}/current"
		end
		@current = "#{config[:path]}/current"
	end

	# Remove temporary directory
	def cleanup
		run "rm -r #{@current}" if @current
	end

	# rm -rf in target directory, then setup
	def nuke
		run "rm -r #{config[:path]}/*"
		setup
	end

	def deploy
		transfer
	end
end