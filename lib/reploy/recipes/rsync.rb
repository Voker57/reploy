# Rsync recipe. Implements sync and copy

class Reploy
	def sync(dir)
		run_local "rsync -avz --delete #{dir}/ #{config[:user]}@#{config[:host]}:#{@current}/"
	end

	def copy
		if config[:copy].is_a? Array
			config[:copy].each do |a|
				k,v = a
				run_local "rsync -tvaz #{k} #{config[:user]}@#{config[:host]}:#{@current}/#{v}"
			end
		end
	end
end