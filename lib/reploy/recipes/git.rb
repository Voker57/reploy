# Git recipe for Reploy

class Reploy
	def git_ref
		if config[:git].has_key? :ref
			config[:git][:ref]
		else
			"HEAD"
		end
	end
	def checkout
		raise "That really shouldn't happen." if Reploy[:git].has_key? :from
		temp_name = "#{Dir.tmpdir}/reploy#{rand(66666)}"
		FileUtils.mkdir_p temp_name
		`git archive #{git_ref} | tar xf - -C #{temp_name}`
		yield temp_name
		FileUtils.rm_rf temp_name
	end
	if Reploy[:git].has_key? :from
		def prepare
			@current = "#{config[:path]}/tmp#{rand(66666)}"
			run "mkdir -p #{@current}"
		end
		def transfer
			begin
				prepare
				# A handy optimization, if repo is local, we can do checkout instead of cloning
				if Reploy[:git][:from] =~ %r{^/}
					run_in_current "GIT_DIR='#{Reploy[:git][:from]}' git archive #{git_ref} | tar xf -"
				else
					run "export GIT_DIR='#{config[:path]}/storage/.git' && (test -d #{config[:path]}/storage/.git && git fetch) || (mkdir -p #{config[:path]}/storage/.git && git clone --bare) && git archive #{git_ref} | tar xf -"
				end
				copy
				finalize
			rescue Exception => e
				puts "Exception: #{e}. Cleaning up..."
				cleanup
			end
		end
	end
end