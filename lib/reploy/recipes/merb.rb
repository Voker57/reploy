class Reploy
	def merb_params_map
		{ :host => "-h",
			:port => "-p",
			:adapter => "-a",
			:environment => "-e"
			}
	end
	def merb_get_args
		(config[:merb] or {}).map do |k,v|
			if merb_params_map[k]
				"#{merb_params_map[k]} #{v}"
			else
				nil
			end
		end.join(" ")
	end

	def restart
		run_in_current "merb -K #{config[:merb][:port]} && merb -d #{merb_get_args}"
	end

	def start
		run_in_current "merb -d #{merb_get_args}"
	end

	def stop
		run_in_current "merb -K #{config[:merb][:port]}"
	end

	def autoupgrade
		run_in_current "MERB_ENV=#{config[:merb][:environment]} rake db:autoupgrade"
	end

	def symlink_shared
		run("ln -s #{config[:path]}/shared/* #{config[:path]}/current/")
	end

	def deploy
		transfer
		symlink_shared
		restart
	end
end

# recipe-specific tasks

desc "Perform Merb schema autoupgrade"
task "reploy:merb:autoupgrade" do
	Reploy.autoupgrade
end

desc "Deploy Merb"
task "reploy:merb:deploy" do
	Reploy.deploy
end