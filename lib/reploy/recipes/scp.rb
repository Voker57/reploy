require 'net/scp'

class Reploy

	def prepare
		@current = "#{config[:path]}/tmp#{rand(66666)}"
		run "mkdir -p #{@current}"
	end

	def sync(dir)
		if config[:scp].has_key? :tar
			if %w|gz gzip|.include? config[:scp][:tar]
				pipes = ["| gzip", "| gunzip" ]
			elsif %w|bz2 bzip2|.include? config[:scp][:tar]
				pipes = ["| bzip2", "| bunzip2" ]
			elsif %w|lzma|.include? config[:scp][:tar]
				pipes = ["| lzma", "| unlzma" ]
			else
				pipes = ["",""]
			end
			archive_name = "reploy#{rand(66666)}.tmp"
			run_local "cd #{dir} && tar c . #{pipes[0]} > #{Dir.tmpdir}/#{archive_name}"
			run_in_current "rm -r *"
			Net::SCP.upload!(config[:host], config[:user],"#{Dir.tmpdir}/#{archive_name}", "#{config[:path]}/current")
			run_in_current "cat #{archive_name} #{pipes[1]} | tar xf -"
			run_in_current "rm #{archive_name}"
		else
			run_in_current "rm -r *"
			Net::SCP.upload!(config[:host], config[:user],"#{Dir.tmpdir}/#{archive_name}", "#{config[:path]}/current", :recursive)
		end
	end

	def copy(files)
		files.each do |a|
			k,v = a
			Net::SCP.upload!(config[:host], config[:user],k, "#{config[:path]}/current/#{v}")
		end
	end
end