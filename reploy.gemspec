spec = Gem::Specification.new do |s|
	s.name = 'reploy'
	s.version = '0.0.1'
	s.summary = 'Working deploying tool'
	s.description = 'Simple tool for automation deployment'
	s.add_dependency('popen4','>= 0.1.2')
	s.add_dependency('open4','>= 0.9.6')
	s.add_dependency('net-ssh', '>= 2.0.11')
	s.add_dependency('net-scp', '>= 1.0.2')
	s.files = Dir['lib/**/*.rb']
	s.require_path = 'lib'
	s.has_rdoc = true
	s.author = "Voker57"
	s.email = "voker57@gmail.com"
	s.homepage = "http://bitcheese.net/wiki/reploy"
end